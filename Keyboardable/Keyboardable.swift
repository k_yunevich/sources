//
//  Keyboardable.swift
//  Keyboardable
//
//  Created by Konstantin Yunevich on 27.02.2018.
//  Copyright © 2018 Konstantin Yunevich. All rights reserved.
//

import UIKit

enum KeyboardNotification {
    case didShow(notification: Notification)
    case didHide(notification: Notification)
    case willShow(notification: Notification)
    case willHide(notification: Notification)
}

// MARK: - Keyboardable protocol declaration
protocol Keyboardable: class {
    func keyboard(notification: KeyboardNotification)
}

private var keyboardNotificationsKey: UInt8 = 1

extension Keyboardable {
    private var observers: [NSObjectProtocol]? {
        get {
            return objc_getAssociatedObject(self, &keyboardNotificationsKey) as? [NSObjectProtocol]
        }
        
        set {
            return objc_setAssociatedObject(self,
                                            &keyboardNotificationsKey,
                                            newValue,
                                            .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func addKeyboardable() {
        let didShowObserver = addObserver(name: .UIKeyboardDidShow) { [weak self] (notification) in
            guard let `self` = self else { return }
            self.keyboard(notification: .didShow(notification: notification))
        }
        
        let didHideObserver = addObserver(name: .UIKeyboardDidHide) { [weak self] (notification) in
            guard let `self` = self else { return }
            self.keyboard(notification: .didHide(notification: notification))
        }
        
        let willShowObserver = addObserver(name: .UIKeyboardWillShow) { [weak self] (notification) in
            guard let `self` = self else { return }
            self.keyboard(notification: .willShow(notification: notification))
        }
        
        let willHideObserver = addObserver(name: .UIKeyboardWillHide) { [weak self] (notification) in
            guard let `self` = self else { return }
            self.keyboard(notification: .willHide(notification: notification))
        }
        
        observers = [didShowObserver,
                     didHideObserver,
                     willShowObserver,
                     willHideObserver]
    }
    
    func removeKeyboardable() {
        guard let observers = observers else { return }
        for observer in observers {
            NotificationCenter.default.removeObserver(observer)
        }
    }
    
    private func addObserver(name: Notification.Name, using: @escaping (Notification) -> Void) -> NSObjectProtocol {
        return NotificationCenter.default.addObserver(forName: name,
                                                      object: nil,
                                                      queue: nil,
                                                      using: using)
    }
}

// MARK: - Notification extention
private extension Notification {
    var keyboardSize: CGSize {
        guard let info = self.userInfo as? [String: AnyObject]
              else { return .zero }
        guard let rect = info[UIKeyboardFrameEndUserInfoKey]?.cgRectValue
              else { return .zero }
        return rect.size
    }
    
    var duration: Double {
        guard let info = self.userInfo as? [String: AnyObject] else { return 0.0 }
        guard let duration = info[UIKeyboardAnimationDurationUserInfoKey]?.doubleValue
              else { return 0.0}
        return duration
    }
}
