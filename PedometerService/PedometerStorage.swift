//
//  PedometerStorage.swift
//  pi
//
//  Created by Konstantin Yunevich on 15.03.2018.
//  Copyright © 2018 AGIMA. All rights reserved.
//

import Foundation

private let uidStorageKey           = "_Pedometer_idStorageKey"
private let activeFromStorageKey    = "_Pedometer_activeFromStorageKey"
private let activeToStorageKey      = "_Pedometer_activeToStorageKey"
private let lastUpdateStorageKey    = "_Pedometer_lastStepsUpdate"
private let lastSendStepsRequestTimestampStorageKey    = "_Pedometer_lastSendStepsRequestTimestamp"
private let isUpdatingKey           = "_Pedometer_isUpdating"
private let lastCompetitionIdKey    = "_Pedometer_lastCompetitionIdKey"

final class PedometerStorage: PedometerStorageProtocol {
    fileprivate let userDefaults = UserDefaults.standard
    
    var uid: Int? {
        set {
            userDefaults.set(newValue,
                             forKey: uidStorageKey)
        }
        
        get {
            let value = userDefaults.value(forKey: uidStorageKey) as? Int
            return value
        }
    }
    
    var activeFrom: Date? {
        set {
             userDefaults.set(newValue,
                              forKey: activeFromStorageKey)
        }
        
        get {
            let value = userDefaults.value(forKey: activeFromStorageKey) as? Date
            return value
        }
    }
    
    var activeTo: Date? {
        set {
            userDefaults.set(newValue, forKey: activeToStorageKey)
        }
        
        get {
            return userDefaults.value(forKey: activeToStorageKey) as? Date
        }
    }
    
    var lastUpdate: Date? {
        set {
            if let uid = uid {
                userDefaults.set(newValue, forKey: lastUpdateStorageKey + "_\(uid)")
            }
        }
        
        get {
            guard let uid = uid else { return nil }
            return userDefaults.value(forKey: lastUpdateStorageKey + "_\(uid)") as? Date
        }
    }

    var lastSendStepsRequestTimestamp: Date? {
        set {
            if let uid = uid {
                userDefaults.set(newValue, forKey: lastSendStepsRequestTimestampStorageKey + "_\(uid)")
            }
        }

        get {
            guard let uid = uid else { return nil }
            return userDefaults.value(forKey: lastSendStepsRequestTimestampStorageKey + "_\(uid)") as? Date
        }
    }
    
    var isUpdating: Bool {
        set {
            if let uid = uid {
                userDefaults.set(newValue, forKey: isUpdatingKey + "_\(uid)")
            }
        }
        
        get {
            guard let uid = uid else { return false }
            return userDefaults.value(forKey: isUpdatingKey + "_\(uid)") as? Bool ?? false
        }
    }
    
    var lastCompetitionId: Int? {
        set {
            if let uid = uid {
                userDefaults.set(newValue, forKey: lastCompetitionIdKey + "_\(uid)")
            }
        }
        
        get {
            guard let uid = uid else { return nil }
            return userDefaults.value(forKey: lastCompetitionIdKey + "_\(uid)") as? Int
        }
    }
    
    func clear() {
        userDefaults.removeObject(forKey: uidStorageKey)
        userDefaults.removeObject(forKey: activeFromStorageKey)
        userDefaults.removeObject(forKey: activeToStorageKey)
        userDefaults.removeObject(forKey: lastSendStepsRequestTimestampStorageKey)
    }
    
    func clearActive() {
        userDefaults.removeObject(forKey: activeFromStorageKey)
        userDefaults.removeObject(forKey: activeToStorageKey)
    }
}
