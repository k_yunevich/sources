//
//  PedometerService.swift
//  pi
//
//  Created by Konstantin Yunevich on 15.03.2018.
//  Copyright © 2018 AGIMA. All rights reserved.
//

import Foundation

protocol PedometerStorageProtocol: class {
    var uid               : Int?  { get set }
    var activeFrom        : Date? { get set }
    var activeTo          : Date? { get set }
    var lastUpdate        : Date? { get set }
    var isUpdating        : Bool { get set }
    var lastCompetitionId : Int? { get set }
    var lastSendStepsRequestTimestamp: Date? { get set }
    func clear()
    func clearActive()
}

protocol PedometerRequestServiceProtocol: class {
    func userInfo(success: @escaping Success<User>, failure: @escaping Failure)
    func send(steps: Int,
              success: @escaping (() -> Void),
              failure: @escaping (NSError) -> Void)
}

class PedometerService {
    fileprivate var requestService: PedometerRequestServiceProtocol
    fileprivate var storageService: PedometerStorageProtocol
    fileprivate let pedometer = Pedometer()

    static let fetchQueue: OperationQueue = {
        let operationQueue = OperationQueue()
        operationQueue.maxConcurrentOperationCount = 1
        return operationQueue
    }();
    
    init(requestService: PedometerRequestServiceProtocol,
         storageService: PedometerStorageProtocol) {
        self.requestService = requestService
        self.storageService = storageService
    }
    
    func setStorage(uid: Int?, activeFrom: Date?, activeTo: Date?) {
        storageService.uid = uid
        storageService.activeFrom = activeFrom
        storageService.activeTo = activeTo
    }
    
    func clearStorage() {
        storageService.clear()
    }
    
    func clearActive() {
        storageService.clearActive()
    }
    
    func fetchUserAndUpdateSteps(completion: @escaping (_ user: User?, _ updated: Bool, _ error: NSError?) -> Void) {
        Logger.instance.log(message: "Fetching steps")

        let operation = DRAsyncBlockOperation { [weak self] (finish) in
            guard let `self` = self else { return }
            
            Logger.instance.log(message: "Sending old steps")
            self.sendSteps(completion: { (success, error) in
                
                Logger.instance.log(message: "Sending new steps")
                self.requestService.userInfo(success: { [weak self] (user) in
                    guard let `self` = self else { return }
                    
                    self.setStorage(uid: user.uid,
                                    activeFrom: user.pedometer?.activeFrom,
                                    activeTo: user.pedometer?.activeTo)
                    
                    if let lastCompetitionId = self.storageService.lastCompetitionId, lastCompetitionId == user.pedometer?.uid {
                        //Do nothing
                    } else {
                        Logger.instance.log(message: "Setting new competititon id \(user.pedometer?.uid ?? -1)")
                        self.storageService.lastCompetitionId = user.pedometer?.uid
                        self.storageService.isUpdating = true
                        self.storageService.lastUpdate = nil
                        self.storageService.lastSendStepsRequestTimestamp = nil
                    }
                    
                    self.sendSteps(completion: { (success, error) in
                        completion(user, true, nil)
                        finish?()
                    })
                    
                }, failure: { (error) in
                    Logger.instance.log(message: "Error when fetching user \"\(error.localizedDescription)\"")
                    completion(nil, false, error)
                    finish?()
                })
            })
        }
        
        
        PedometerService.fetchQueue.addOperation(operation!)
    }
    
    func sendSteps(completion: @escaping (Bool, NSError?) -> Void) {
        let currentDate = Date()
        pedometer.querySteps(from: Date(), to: Date(), completion: { (_, _) in })

        guard let activeTo = storageService.activeTo else {
            Logger.instance.log(message: "There is no active competition, stopping")
            completion(false, nil)
            return
        }

        var fromDate: Date

        guard let activeFrom = storageService.activeFrom else {
            Logger.instance.log(message: "There is no active competition, stopping")
            completion(false, nil)
            return
        }
        
        if let lastUpdate = storageService.lastUpdate, lastUpdate > activeFrom {
            if lastUpdate > activeTo {
                Logger.instance.log(message: "Competition has ended, stopping")
                completion(false, nil)
                return
            }
            fromDate = lastUpdate
        } else if activeFrom > activeTo  {
            Logger.instance.log(message: "activeFrom greater that activeTo, stopping")
            completion(false, nil)
            return
        } else if currentDate >= activeFrom && currentDate <= activeTo {
            fromDate = activeFrom
        } else if storageService.isUpdating {
            Logger.instance.log(message: "Last sending in competition")
            storageService.isUpdating = false
            fromDate = activeFrom
        } else {
            Logger.instance.log(message: "Competition has ended without lastUpdate, stopping")
            completion(false, nil)
            return
        }
        
        pedometer.querySteps(from: fromDate, to: activeTo, completion: { [weak self] (steps, error) in
            guard let `self` = self else {
                completion(false, nil)
                return
            }
            
            if let error = error {
                Logger.instance.log(message: "Error fetching steps \"\(error.localizedDescription)\", stopping")
                completion(false, error as NSError)
                return
            }
            
            guard let steps = steps, steps > 0 else {
                Logger.instance.log(message: "0 steps, stopping")
                completion(false, nil)
                return
            }
            
            Logger.instance.log(message: "Sending steps \(steps) from \(Logger.instance.dateString(fromDate)) to \(Logger.instance.dateString(activeTo)).")
            
            self.requestService.send(steps: steps, success: { [weak self] in
                Logger.instance.log(message: "Steps sent")
                guard let `self` = self else { return }
                self.storageService.lastUpdate = currentDate
                completion(true, nil)
            }, failure: { (error) in
                Logger.instance.log(message: "Error when sending steps \"\(error.localizedDescription)\"")
                completion(false, error)
            })
        })
    }
}
