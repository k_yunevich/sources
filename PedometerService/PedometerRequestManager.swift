//
//  PedometerRequestManager.swift
//  pi
//
//  Created by Konstantin Yunevich on 15.03.2018.
//  Copyright © 2018 AGIMA. All rights reserved.
//

import Foundation

final class PedometerRequestManager: BaseRequestManager {
    
}

extension PedometerRequestManager: PedometerRequestServiceProtocol {
    func send(steps: Int, success: @escaping (() -> Void), failure: @escaping (NSError) -> Void) {
        let stepsUrl = Urls.Steps.sendSteps + "\(steps)/"
        
        requestManager.makeRequestWithoutMapping(URL: stepsUrl,
                                                 method: .post,
                                                 failure: failure,
                                                 success: { (_) in
                                                    success()
        })
    }
    
    func userInfo(success: @escaping Success<User>,
                  failure: @escaping Failure) {
        requestManager.makeRequestObject(URL: Urls.User.info,
                                         method: .get,
                                         keyPath: "result",
                                         failure: failure,
                                         success: success)
    }
}
