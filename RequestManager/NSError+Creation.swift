//
//  NSError+Creation.swift
//  Konstantin Yunevich
//
//  Created by Konstantin Yunevich on 28.09.17.
//  Copyright © 2016 Konstantin Yunevich. All rights reserved.
//

import Foundation

extension NSError {
    static func error(with errorType: ErrorType) -> NSError {
        return error(title: errorType.rawValue)
    }
    
    public static func error(title: String, code: Int) -> NSError {
        let bundleName =  Bundle.main.bundleIdentifier!
        return NSError(domain: bundleName, code: code, userInfo: [NSLocalizedDescriptionKey : title])
    }
    
    public static func error(title: String) -> NSError {
        return error(title: title, code: 0)
    }
    
}
