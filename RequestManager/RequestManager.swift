//
//  RequestManager.swift
//  Konstantin Yunevich
//
//  Created by Konstantin Yunevich on 28.09.17.
//  Copyright © 2017 Konstantin Yunevich. All rights reserved.
//

import Foundation

import Alamofire
import AlamofireObjectMapper
import ObjectMapper

public typealias Parameters = [String : Any]
public typealias Headers = [String : String]
public typealias Failure = (NSError) -> ()
public typealias Success<T> = (T) -> ()
public typealias SuccessNoMapping = Success<[String : Any]?>

open class RequestManager {
    
    open static let shared: RequestManager = {
        return RequestManager()
    }()
    
    open static func setup(configuration: RequestManagerConfiguration) {
        RequestManager.shared.setup(configuration: configuration)
    }
    
    open func setup(configuration: RequestManagerConfiguration) {
        requestManagerConfigure(configuration)
    }

    open var configuration: RequestManagerConfiguration {
        return _configuration
    }
    
    // MARK: - Private methods
    fileprivate var requestManager: SessionManager!
    
    private func requestManagerConfigure(_ configuration: RequestManagerConfiguration) {
        self._configuration = configuration
        
        let config: URLSessionConfiguration
        if let remoteConf = configuration.sessionConfig {
            config = remoteConf
        } else {
            config = URLSessionConfiguration.default
        }
        config.timeoutIntervalForRequest = configuration.timeout
        config.timeoutIntervalForResource = configuration.timeout
        self.requestManager = SessionManager(configuration: config, delegate: configuration.sessionDelegate ?? SessionDelegate())
        self.requestManager.startRequestsImmediately = true
    }
    
    private init() {}
    
    public init(configuration: RequestManagerConfiguration) {
        requestManagerConfigure(configuration)
    }
    
    fileprivate var _configuration: RequestManagerConfiguration!
    
    fileprivate func configure(parameters: Parameters?) -> Parameters {
        var params: Parameters = _configuration.defaultParams ?? [:]
        
        if let parameters = parameters {
            params += parameters
        }
        
        return params
    }
    
    fileprivate func configure(headers: Headers?) -> Headers? {
        var _headers: Headers = _configuration.defaultHeaders ?? [:]
        if let headers = headers {
            _headers += headers
        }
        return _headers
    }
    
    public func prepareRequest(
        URL: String,
        method: HTTPMethod,
        parameters: Parameters?,
        headers: Headers?,
        encoding: ParameterEncoding?) -> DataRequest
    {
        let fullUrl = _configuration.baseURL + URL
        let headers = configure(headers: headers)
        let params = configure(parameters: parameters)
        let request = requestManager.request(fullUrl, method: method, parameters: params, encoding: encoding ?? _configuration.encoding,
                headers: headers)
        if _configuration.logging {
            print("Request: \(request.debugDescription)")
        }
        return request
    }
    
    fileprivate func log<T>(_ response: DataResponse<T>) {
        if _configuration.logging {
            print("Response: \(response.response.debugDescription)")
            if let jsonString = jsonString(fromData: response.data!) {
                print("JSON: \(jsonString)")
            } else {
                print("JSON: No data")
            }
        }
    }
    
    fileprivate func jsonString(fromData: Data) -> String? {
        return String(data: fromData, encoding: String.Encoding.utf8)
    }
    
    public func handleError<T>(_ response: DataResponse<T>) -> NSError? {
        let result = response.toJSON()
        
        if let resultValue = result, let errors = resultValue["errors"] as? [String], errors.count > 0 {
            guard let statusCode = resultValue["status"] as? Int else { return nil }
            
            let errorType = errors[0]
            let errorTitle = ErrorType.create(with: errorType)
            return NSError.error(title: errorTitle.rawValue, code: statusCode)
        } else {
            if let _ = response.result.error {
                if let statusCode = response.response?.statusCode {
                    return NSError.error(title: ErrorType.unknown.rawValue, code: statusCode)
                }else {
                    return NSError.error(title: ErrorType.unknown.rawValue, code: -1)
                }
            }
        }
        
        return nil
    }
    
    // MARK: - Public methods
    
    @discardableResult open func makeRequestWithoutMapping(
        URL: String,
        method: HTTPMethod = .post,
        encoding: ParameterEncoding? = nil,
        parameters: Parameters? = nil,
        headers: Headers? = nil,
        failure: @escaping Failure,
        success: @escaping Success<Any>) -> DataRequest {
        let request = prepareRequest(URL: URL,
                method: method,
                parameters: parameters,
                headers: headers,
                encoding: encoding)
        request.responseJSON { [weak self] (response) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.log(response)
            let error = strongSelf.handleError(response)

            if let error = error {
                failure(error)
            } else {
                if let response = response.result.value {
                    success(response)
                }
                else {
                    failure(strongSelf.configuration.nilResponseError)
                }
            }
        }
        return request
    }
    
    @discardableResult open func makeRequestObject<T: Mappable>(
        URL: String,
        method: HTTPMethod = .post,
        encoding: ParameterEncoding? = nil,
        parameters: Parameters? = nil,
        headers: Headers? = nil,
        keyPath: String,
        failure: @escaping Failure,
        success: @escaping Success<T>,
        successNoMapping: SuccessNoMapping? = nil) -> DataRequest {
        let request = prepareRequest(URL: URL,
                method: method,
                parameters: parameters,
                headers: headers,
                encoding: encoding)
        request.responseObject(queue: nil, keyPath: keyPath, mapToObject: nil, context: nil) { [weak self] (response: DataResponse<T>) in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.log(response)
            let error = strongSelf.handleError(response)

            if let error = error {
                failure(error)
            } else {
                if let response = response.result.value {
                    success(response)
                }
                else {
                    failure(strongSelf.configuration.nilResponseError)
                }
                
                if let noMapping = successNoMapping {
                    if let result = response.toJSON() {
                        noMapping(result)
                    } else {
                        noMapping(nil)
                    }
                }
            }
        }
        return request
    }
    
    @discardableResult open func makeRequest<T: Mappable>(
        URL: String,
        method: HTTPMethod = .post,
        encoding: ParameterEncoding? = nil,
        parameters: Parameters? = nil,
        headers: Headers? = nil,
        keyPath: String,
        failure: @escaping Failure,
        success: @escaping Success<[T]>,
        successNoMapping: SuccessNoMapping? = nil) -> DataRequest {
        let request = prepareRequest(URL: URL,
                method: method,
                parameters: parameters,
                headers: headers,
                encoding: encoding)
        request.responseArray(queue: nil, keyPath: keyPath, context: nil) { [weak self] (response: DataResponse<[T]>) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.log(response)
            let error = strongSelf.handleError(response)

            if let error = error {
                failure(error)
            } else {
                if let response = response.result.value {
                    success(response)
                }
                else {
                    failure(strongSelf.configuration.nilResponseError)
                }
                
                if let noMapping = successNoMapping {
                    if let result = response.toJSON() {
                        noMapping(result)
                    } else {
                        noMapping(nil)
                    }
                }
            }
        }
        
        return request
    }
}

func += <KeyType, ValueType> (left: inout Dictionary<KeyType, ValueType>, right: Dictionary<KeyType, ValueType>) {
    for (k, v) in right {
        left.updateValue(v, forKey: k)
    }
}

extension DataResponse {
    func toJSON() -> [String : Any]? {
        let JSONSerializer = DataRequest.jsonResponseSerializer(options: JSONSerialization.ReadingOptions.allowFragments)
        let resultDict = JSONSerializer.serializeResponse(self.request, self.response, self.data, nil)
        return resultDict.value as? [String : Any]
    }
}

