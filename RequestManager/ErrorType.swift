//
//  ErrorType.swift
//  Konstantin Yunevich
//
//  Created by Konstantin Yunevich on 23.01.2018.
//  Copyright © 2018 Konstantin Yunevich. All rights reserved.
//

import Foundation

enum ErrorType: String {
    case loginEmpty             = "Введите логин"
    case passwordEmpty          = "Введите пароль"
    case invalidLoginOrPassword = "Неверный логин или пароль"
    case unknown                = "Извините, но что-то пошло не так"
    case emailInvalid           = "Некорректный e-mail"
    case passwordInvalid        = "Некорректный пароль"
    case notInternet            = "Интернет недоступен, повторите попытку позже"
    
    static func create(with errorString: String) -> ErrorType {
        switch errorString {
        case "Incorrect login or password":
            return .invalidLoginOrPassword
        case "Password is empty":
            return .passwordEmpty
        case "Login is empty":
            return .loginEmpty
        default:
            return .unknown
        }
    }
}
