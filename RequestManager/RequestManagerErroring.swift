//
//  RequestManagerErroring.swift
//  PedometeriOS
//
//  Created by Konstantin Yunevich on 23.01.2018.
//  Copyright © 2018 Konstantin Yunevich. All rights reserved.
//

import Foundation
import ObjectMapper

public protocol RequestManagerErrorMapping: Mappable {
    var status: String? { get set }
    var errors: [String]? { get set }
}

public protocol RequestManagerErroring {
    var description: String? { get }
    var errorCode: Int { get }
    func defaultError() -> Self
    func description(code: Int) -> String?
}
