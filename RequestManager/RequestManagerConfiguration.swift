//
//  RequestManagerConfiguration.swift
//  Konstantin Yunevich
//
//  Created by Konstantin Yunevich on 28.09.17.
//  Copyright © 2017 Konstantin Yunevich. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire

public typealias ErrorMapping = (Any) -> (RequestManagerErroring?)
public typealias HttpErrorHandler = (Int?) -> ()

public struct RequestManagerConfiguration {
    public var baseURL: String
    
    public var defaultParams: Parameters?
    public var defaultHeaders: Headers?
    public var timeout: TimeInterval = 10
    public var logging: Bool = false
    public var errorMap: ErrorMapping?
    public var nilResponseError: NSError
    public var encoding: ParameterEncoding
    public var httpErrorHandler: ((Int?) -> ())?
    public var sessionDelegate: Alamofire.SessionDelegate?
    public var sessionConfig: URLSessionConfiguration?
    
    public init(baseURL: String,
                params: Parameters? = nil,
                headers: Headers? = nil,
                timeout: TimeInterval = 10,
                encoding: ParameterEncoding = URLEncoding.default,
                logging: Bool = false,
                nilResponseError: NSError = NSError.error(title: "Проверьте Ваше интернет соединение",
                                                          code: -11),
                httpErrorHanlder: HttpErrorHandler? = nil,
                errorMap: ErrorMapping? = nil,
                sessionDelegate: Alamofire.SessionDelegate? = nil,
                sessionConfig: URLSessionConfiguration? = nil)
    {
        self.baseURL = baseURL
        self.defaultParams = params
        self.defaultHeaders = headers
        self.timeout = timeout
        self.encoding = encoding
        self.logging = logging
        self.nilResponseError = nilResponseError
        self.errorMap = errorMap
        self.httpErrorHandler = httpErrorHanlder
        self.sessionDelegate = sessionDelegate
        self.sessionConfig = sessionConfig
    }
}
