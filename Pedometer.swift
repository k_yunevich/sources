//
//  Pedometer.swift
//  pi
//
//  Created by Konstantin Yunevich on 10.03.2018.
//  Copyright © 2018 AGIMA. All rights reserved.
//

import Foundation
import CoreMotion

typealias PedometerHandler = ((_ steps: Int?, _ error: Error?) -> Void)

enum PedometerActivity {
    case walking
    case running
}

class Pedometer {
    fileprivate var _pedometer: CMPedometer
    fileprivate var _activityManager: CMMotionActivityManager
    
    fileprivate var isAvailableStepsCounting: Bool {
        return CMPedometer.isStepCountingAvailable() && CMMotionActivityManager.isActivityAvailable()
    }
    
    init() {
        self._activityManager = CMMotionActivityManager()
        self._pedometer = CMPedometer()
    }
        
    func trackingSteps(with activities:[PedometerActivity],
                       from: Date,
                       handler: @escaping PedometerHandler) {
        if isAvailableStepsCounting {
            _activityManager.startActivityUpdates(to: OperationQueue.main, withHandler: { [weak self] (activity) in
                guard let `self` = self else { return }
                guard let activity = activity else { return }
                
                if activity.walking && activities.contains(.walking)
                    || activity.running && activities.contains(.running) {
                    self.querySteps(from: from, to: Date(), completion: { (steps, error) in
                        handler(steps, error)
                    })
                }
            })
        } else {
            handler(nil, nil)
        }
    }
    
    func trackingSteps(from date: Date, handler: @escaping PedometerHandler) {
        if isAvailableStepsCounting {
            _pedometer.startUpdates(from: date) { (data, error) in
                DispatchQueue.main.async {
                    let numberOfSteps = data?.numberOfSteps.intValue
                    handler(numberOfSteps, error)
                }
            }
        } else {
            handler(nil, nil)
        }
    }
    
    func querySteps(from: Date, to: Date, completion: @escaping PedometerHandler) {
        if isAvailableStepsCounting {
            _pedometer.queryPedometerData(from: from, to: to, withHandler: { (data, error) in
                DispatchQueue.main.async {
                    let numberOfSteps = data?.numberOfSteps.intValue
                    completion(numberOfSteps, error)
                }
            })
        } else {
            completion(nil, nil)
        }
    }
    
    func stopTracking() {
        _activityManager.stopActivityUpdates()
        _pedometer.stopUpdates()
    }
}
