//
//  KYSelectAlertTableViewCell.h
//  KYAlertController
//
//  Created by Konstantin Yunevich on 04.02.2017.
//  Copyright © 2017 Konstantin Yunevich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KYSelectAlertTableViewCell : UITableViewCell

+ (CGFloat)cellHeightWithContent:(NSString *)content;
- (void)setupWithTitle:(NSString *)title isSelected:(BOOL)selected;

- (void)showSeparator;
- (void)hideSeparator;
- (void)setCheckmarkEnabled:(BOOL)enabled;

@end
