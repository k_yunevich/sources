//
//  KYSelectAlertTableViewCell.m
//  KYAlertController
//
//  Created by Konstantin Yunevich on 04.02.2017.
//  Copyright © 2017 Konstantin Yunevich. All rights reserved.
//

#import "KYSelectAlertTableViewCell.h"
#import "NSString+Size.h"

@interface KYSelectAlertTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *checkmarkImageView;
@property (weak, nonatomic) IBOutlet UIView *separatorView;

@end

@implementation KYSelectAlertTableViewCell

+ (CGFloat)cellHeightWithContent:(NSString *)content {
    if (content.length == 0) {
        return 0;
    }
    
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    CGFloat width = screenSize.width - 35;
    CGFloat height = [content heightForWidth:width font:[UIFont systemFontOfSize:16]];
    return height + 41;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self configureCell];
    [self setCheckmarkEnabled:NO];
}

- (void)setCheckmarkEnabled:(BOOL)enabled {
    if (enabled) {
        UIImage *image = [UIImage imageNamed:@"im_check_on"];
        self.checkmarkImageView.image = image;
    } else {
        self.checkmarkImageView.image = [UIImage imageNamed:@"im_check"];
    }
}

- (void)showSeparator {
    self.separatorView.hidden = NO;
}

- (void)hideSeparator {
    self.separatorView.hidden = YES;
}

#pragma mark - Setup cell
- (void)setupWithTitle:(NSString *)title isSelected:(BOOL)selected {
    [self setupTitleLabelWithText:title];
}

- (void)setupTitleLabelWithText:(NSString *)text {
    NSDictionary *attributes = @{ NSKernAttributeName : @(0.2),
                                  NSFontAttributeName: [UIFont systemFontOfSize:17]
                                  };
    
    self.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:text
                                                                     attributes:attributes];
}

#pragma mark - Cell Configuration
- (void)configureCell {
   
}

@end
