//
//  KYSelectAlertControllerDataSource.h
//  KYAlertController
//
//  Created by Konstantin Yunevich on 04.02.2017.
//  Copyright © 2017 Konstantin Yunevich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KYSelectAlertTableViewCell.h"
#import "KYSelectAlertAction.h"
#import "UITableView+Nib.h"

typedef void(^KYSelectAlertControllerRowAction)(NSInteger index);

@interface KYSelectAlertControllerDataSource : NSObject <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, getter=isAllowsMultipleSelection) BOOL allowsMultipleSelection;
@property (strong, nonatomic, nonnull) NSMutableArray<NSIndexPath *> *selectedIndexPaths;
@property (copy, nonatomic, nonnull) KYSelectAlertControllerRowAction selectRowAction;
- (void)refreshItems:(NSArray<KYSelectAlertAction *> * _Nonnull)items;

@end
