//
//  KYSelectAlertControllerDataSource.m
//  KYAlertController
//
//  Created by Konstantin Yunevich on 04.02.2017.
//  Copyright © 2017 Konstantin Yunevich. All rights reserved.
//

#import "KYSelectAlertControllerDataSource.h"

@interface KYSelectAlertControllerDataSource ()

@property (strong, nonatomic) NSArray<KYSelectAlertAction *> *items;

@end

@implementation KYSelectAlertControllerDataSource

- (instancetype)init
{
    self = [super init];
    if (self) {
        _selectedIndexPaths = [NSMutableArray array];
    }
    return self;
}


- (void)refreshItems:(NSArray<KYSelectAlertAction *> *)items {
    self.items = [NSArray arrayWithArray:items];
}

#pragma mark - UITableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    KYSelectAlertTableViewCell *cell = [self returnContactsAlertCell:tableView
                                                             indexPath:indexPath];
    KYSelectAlertAction *action = self.items[indexPath.row];
    [cell setupWithTitle:action.title isSelected:NO];
    
    if ([self.selectedIndexPaths containsObject:indexPath]) {
        [cell setCheckmarkEnabled:YES];
    } else {
        [cell setCheckmarkEnabled:NO];
    }
    
    return cell;
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    KYSelectAlertAction *action = self.items[indexPath.row];
    return [KYSelectAlertTableViewCell cellHeightWithContent:action.title];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    KYSelectAlertTableViewCell *cell;
    cell = (KYSelectAlertTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    if ([self.selectedIndexPaths containsObject:indexPath]) {
        if (self.isAllowsMultipleSelection) {
            [self.selectedIndexPaths removeObject:indexPath];
            [cell setCheckmarkEnabled:NO];
        }
    } else {
        if (!self.isAllowsMultipleSelection && self.selectedIndexPaths.count >= 1) {
            NSIndexPath *indexPath = [self.selectedIndexPaths objectAtIndex:0];
            KYSelectAlertTableViewCell *cell;
            cell = (KYSelectAlertTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
            [cell setCheckmarkEnabled:NO];
            [self.selectedIndexPaths removeObject:indexPath];
        }
        
        [self.selectedIndexPaths addObject:indexPath];
        [cell setCheckmarkEnabled:YES];
    }
    
    KYSelectAlertAction *action = self.items[indexPath.row];
    
    if (action.actionHandler) {
        action.actionHandler();
    }
    
    self.selectRowAction(indexPath.row);
}

#pragma mark - Helpers
- (KYSelectAlertTableViewCell *)returnContactsAlertCell:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    KYSelectAlertTableViewCell *cell;
    cell = (KYSelectAlertTableViewCell *)[tableView dequeueReusableCell:[KYSelectAlertTableViewCell class]
                                                           forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSInteger numberOfRows = [self tableView:tableView
                       numberOfRowsInSection:indexPath.section];
    if (indexPath.row == numberOfRows - 1) {
        [cell hideSeparator];
    } else {
        [cell showSeparator];
    }
    
    return cell;
}

@end
