//
//  KYSelectAlertAction.h
//  KYAlertController
//
//  Created by Konstantin Yunevich on 04.02.2017.
//  Copyright © 2017 Konstantin Yunevich. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^KYSelectAlertActionHandler) (void);

@interface KYSelectAlertAction : NSObject

@property (strong, nonatomic, readonly, nullable) NSString *title;
@property (nonatomic, getter=isSelected, readonly) BOOL selected;
@property (copy, nonatomic, readonly, nullable) KYSelectAlertActionHandler actionHandler;

- (instancetype _Nonnull)initWithTitle:(NSString * _Nullable)title
                            isSelected:(BOOL)selected
                         actionHandler:(KYSelectAlertActionHandler _Nullable)handler;

@end
