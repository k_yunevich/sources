//
//  KYSelectAlertAction.m
//  KYAlertController
//
//  Created by Konstantin Yunevich on 04.02.2017.
//  Copyright © 2017 Konstantin Yunevich. All rights reserved.
//

#import "KYSelectAlertAction.h"

@interface KYSelectAlertAction ()

@property (strong, nonatomic) NSString *title;
@property (nonatomic, getter=isSelected) BOOL selected;
@property (copy, nonatomic, nullable) KYSelectAlertActionHandler actionHandler;

@end

@implementation KYSelectAlertAction

- (instancetype _Nonnull)initWithTitle:(NSString * _Nullable)title
                            isSelected:(BOOL)selected
                         actionHandler:(KYSelectAlertActionHandler _Nullable)handler
{
    self = [super init];
    if (self) {
        _title = title;
        _selected = selected;
        _actionHandler = handler;
    }
    return self;
}

@end
