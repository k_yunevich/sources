//
//  KYSelectAlertController.h
//  KYAlertController
//
//  Created by Konstantin Yunevich on 04.02.2017.
//  Copyright © 2017 Konstantin Yunevich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KYSelectAlertAction.h"

@class KYSelectAlertController;

typedef void(^KYSelectAlertControllerCompletionBlock)(BOOL isDone, NSArray<NSNumber *> *selectedIndexes);

@interface KYSelectAlertController : UIViewController

@property (nonatomic, getter=isAllowsMultipleSelection) BOOL allowsMultipleSelection;

- (instancetype _Nonnull)initWithActions:(NSArray<KYSelectAlertAction *> * _Nonnull)actions
                          title:(NSString * _Nullable)title
                       cancelButtonTitle:(NSString * _Nonnull)cancelTitle
                              completion:(KYSelectAlertControllerCompletionBlock _Nonnull)completion;

- (void)setStartIndexes:(NSArray<NSNumber *> * _Nonnull)selectedIndexes;

@end
