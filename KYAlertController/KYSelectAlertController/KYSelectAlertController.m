//
//  KYSelectAlertController.m
//  KYAlertController
//
//  Created by Konstantin Yunevich on 04.02.2017.
//  Copyright © 2017 Konstantin Yunevich. All rights reserved.
//

#import "KYSelectAlertController.h"
#import "KYSelectAlertControllerDataSource.h"
#import "KYSelectAlertAnimator.h"

static const CGFloat kCancelButtonHeight = 56.0;
static const CGFloat kCancelButtonBottomPadding = 13.0;
static const CGFloat kCancelButtonCornerRadius = 14.0;

static const CGFloat kContainerViewBottomPadding = 7.0;
static const CGFloat kContainerViewLeftPadding = 10.0;
static const CGFloat kContainerViewRightPadding = 10.0;
static const CGFloat kContainerViewCornerRadius = 14.0;
static const CGFloat kContainerViewHeaderHeight = 36.0;
static const CGFloat kContainerViewMaximumY = 85;

@interface KYSelectAlertController () <UIViewControllerTransitioningDelegate>

@property (strong, nonatomic, nullable) NSString *headerTitle;
@property (strong, nonatomic) NSString *cancelTitle;
@property (strong, nonatomic) NSArray<KYSelectAlertAction *> *actions;
@property (strong, nonatomic) KYSelectAlertControllerDataSource *dataSource;
@property (strong, nonatomic) NSMutableArray<NSNumber *> *selectedIndexes;
@property (strong, nonatomic) UIView *containerView;
@property (strong, nonatomic) UIButton *cancelButton;
@property (strong, nonatomic) UIButton *doneButton;
@property (strong, nonatomic) UIView *separatorView;
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UIView *overlayView;
@property (strong, nonatomic) KYSelectAlertAnimator *animator;

@property (copy, nonatomic) KYSelectAlertControllerCompletionBlock completionHandler;

@end

@implementation KYSelectAlertController

#pragma mark - Lifecycle
- (instancetype _Nonnull)initWithActions:(NSArray<KYSelectAlertAction *> * _Nonnull)actions
                                   title:(NSString * _Nullable)title
                       cancelButtonTitle:(NSString * _Nonnull)cancelTitle
                              completion:(KYSelectAlertControllerCompletionBlock _Nonnull)completion
{
    self = [super init];
    if (self) {
        _animator = [[KYSelectAlertAnimator alloc] init];
        self.transitioningDelegate = self;
        _allowsMultipleSelection = YES;
        _headerTitle = title;
        _cancelTitle = cancelTitle;
        _actions = actions;
        _selectedIndexes = [[NSMutableArray alloc] init];
        _completionHandler = completion;
        _dataSource = [[KYSelectAlertControllerDataSource alloc] init];
        self.modalPresentationStyle = UIModalPresentationOverFullScreen;
    }
    return self;
}

- (void)setStartIndexes:(NSArray<NSNumber *> *)selectedIndexes {
    self.selectedIndexes = [NSMutableArray arrayWithArray:selectedIndexes];
    
    for (NSNumber *index in selectedIndexes) {
        [self.dataSource.selectedIndexPaths addObject:[NSIndexPath indexPathForRow:index.integerValue inSection:0]];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureViewController];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self layoutContainerViewFrame];
    [self layoutTableView];
    [self layoutCancelButton];
    [self layoutDoneButton];
    [self layoutSeparatorView];
}

#pragma mark - Configuration
- (CGFloat)contentHeight {
    CGFloat height = 0.0;
    for (KYSelectAlertAction *action in self.actions) {
        height += [KYSelectAlertTableViewCell cellHeightWithContent:action.title];
    }
    
    return height;
}

- (void)configureViewController {
    [self configureContainerView];
    [self configureHeaderView];
    [self configureTableView];
    [self configureCancelButton];
    [self configureDoneButton];
    [self configureDoneSeparatorView];
}

- (void)layoutContainerViewFrame {
    CGSize screenSize = [self screenSize];
    CGFloat height = [self contentHeight] + kContainerViewHeaderHeight + kCancelButtonHeight;
    
    CGFloat containerViewPositionY = self.view.frame.size.height - self.view.frame.origin.y;
    containerViewPositionY -= kCancelButtonBottomPadding;
    containerViewPositionY -= kCancelButtonHeight;
    containerViewPositionY -= kContainerViewBottomPadding;
    containerViewPositionY -= height;
    
    if (containerViewPositionY < kContainerViewMaximumY) {
        containerViewPositionY = kContainerViewMaximumY;
        height = self.view.frame.size.height - self.view.frame.origin.y;
        height -= kCancelButtonBottomPadding;
        height -= kCancelButtonHeight;
        height -= kContainerViewBottomPadding;
        height -= containerViewPositionY;
    }
    
    CGFloat containerViewWidth = screenSize.width - kContainerViewLeftPadding;
    containerViewWidth -= kContainerViewRightPadding;
    CGRect containerViewFrame = CGRectMake(kContainerViewLeftPadding,
                                           containerViewPositionY,
                                           containerViewWidth,
                                           height);
    
    self.containerView.frame = containerViewFrame;
}

- (void)layoutCancelButton {
    CGFloat cancelButtonPositionY = self.view.frame.size.height - self.view.frame.origin.y;
    
    cancelButtonPositionY -= kCancelButtonHeight;
    cancelButtonPositionY -= kCancelButtonBottomPadding;
    
    CGRect cancelButtonFrame = CGRectMake(self.containerView.frame.origin.x,
                                          cancelButtonPositionY,
                                          self.containerView.frame.size.width,
                                          kCancelButtonHeight);
    
    self.cancelButton.frame = cancelButtonFrame;
}

- (void)layoutSeparatorView {
    CGFloat separatorViewPositionY = self.containerView.frame.size.height - self.doneButton.frame.size.height;
    CGRect separatorViewFrame = CGRectMake(0,
                                           separatorViewPositionY,
                                           self.containerView.frame.size.width, 0.5);
    self.separatorView.frame = separatorViewFrame;
}

- (void)layoutDoneButton {
    CGFloat doneButtonPositionY = self.containerView.frame.size.height - kCancelButtonHeight;
    CGRect doneButtonFrame = CGRectMake(0,
                                          doneButtonPositionY,
                                          self.containerView.frame.size.width,
                                          kCancelButtonHeight);
    
    self.doneButton.frame = doneButtonFrame;
}

- (void)layoutTableView {
    CGRect tableViewFrame = CGRectMake(0,
                                       kContainerViewHeaderHeight,
                                       self.containerView.frame.size.width,
                                       self.containerView.frame.size.height - kContainerViewHeaderHeight - kCancelButtonHeight);
    self.tableView.frame = tableViewFrame;
}

- (void)configureContainerView {    
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectZero];
    self.containerView = containerView;
    [self layoutContainerViewFrame];
    containerView.backgroundColor = [UIColor whiteColor];
    containerView.clipsToBounds = YES;
    containerView.layer.cornerRadius = kContainerViewCornerRadius;
    [self.view addSubview:containerView];
}

- (void)configureTableView {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero];
    [tableView registerNib:[KYSelectAlertTableViewCell class]];
    self.dataSource.allowsMultipleSelection = self.allowsMultipleSelection;
    [self.dataSource refreshItems:self.actions];
    [self configureDataSource];
    tableView.dataSource = self.dataSource;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.delegate = self.dataSource;
    tableView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    [tableView reloadData];
    [self.containerView addSubview:tableView];
     self.tableView = tableView;
}

- (void)dealloc {
    
}

- (void)configureDataSource {
    __weak typeof(self) weakSelf = self;
    self.dataSource.selectRowAction = ^(NSInteger index) {
         __strong typeof(self)self = weakSelf;
        NSNumber *numberIndex = @(index);
        if ([self.selectedIndexes containsObject:numberIndex]) {
            if (self.isAllowsMultipleSelection) {
                [self.selectedIndexes removeObject:numberIndex];
            }
        } else {
            if (!self.isAllowsMultipleSelection && self.selectedIndexes.count >= 1) {
                [self.selectedIndexes removeObjectAtIndex:0];
            }
            
            [self.selectedIndexes addObject:numberIndex];
        }
    };
}

- (void)configureDoneSeparatorView {
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    view.backgroundColor = [UIColor colorWithRed:235.0 / 255.0
                                           green:235.0 / 255.0
                                            blue:235.0 / 255.0 alpha:1.0];
    [self.containerView addSubview:view];
    self.separatorView = view;
}

- (void)configureHeaderView {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                  0,
                                                                  self.containerView.bounds.size.width,
                                                                  kContainerViewHeaderHeight)];
    headerView.backgroundColor = [UIColor colorWithRed:245.0 / 255.0
                                                 green:245.0 / 255.0
                                                  blue:245.0 / 255.0
                                                 alpha:1.0];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:headerView.bounds];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    NSRange titleRange = NSMakeRange(0, self.headerTitle.length);
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.headerTitle];
    [attributedString addAttribute:NSKernAttributeName
                             value:@(0.2) range:titleRange];
    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:117.0 / 255.0
                                                                                        green:117.0 / 255.0
                                                                                         blue:117.0 / 255.0
                                                                                        alpha:1.0] range:titleRange];
    [attributedString addAttribute:NSFontAttributeName
                             value:[UIFont systemFontOfSize:15] range:titleRange];
    
    titleLabel.attributedText = attributedString;
    [headerView addSubview:titleLabel];
    [self.containerView addSubview:headerView];
}

- (void)configureCancelButton {
    UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectZero];
    [cancelButton setTitle:self.cancelTitle forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor colorWithRed:0
                                                green:122.0 / 255.0
                                                 blue:255.0 / 255.0
                                                alpha:1.0] forState:UIControlStateNormal];
    cancelButton.titleLabel.font = [UIFont systemFontOfSize:20];
    cancelButton.backgroundColor = [UIColor whiteColor];
    cancelButton.clipsToBounds = YES;
    cancelButton.layer.cornerRadius = kCancelButtonCornerRadius;
    [cancelButton addTarget:self
                     action:@selector(cancelButtonAction)
           forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cancelButton];
    self.cancelButton = cancelButton;
}

- (void)configureDoneButton {
    UIButton *doneButton = [[UIButton alloc] initWithFrame:CGRectZero];
    [doneButton setTitle:@"Готово" forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor colorWithRed:0
                                                green:122.0 / 255.0
                                                 blue:255.0 / 255.0
                                                alpha:1.0] forState:UIControlStateNormal];
    doneButton.titleLabel.font = [UIFont systemFontOfSize:20];
    doneButton.backgroundColor = [UIColor whiteColor];
    doneButton.clipsToBounds = YES;
    doneButton.layer.cornerRadius = kCancelButtonCornerRadius;
    [doneButton addTarget:self
                     action:@selector(doneButtonAction)
           forControlEvents:UIControlEventTouchUpInside];
    [self.containerView addSubview:doneButton];
    self.doneButton = doneButton;
}

#pragma mark - Actions
- (void)cancelButtonAction {
    self.completionHandler(NO, self.selectedIndexes);
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)doneButtonAction {
    self.completionHandler(YES, self.selectedIndexes);
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Helpers
- (CGSize)screenSize {
    return [UIScreen mainScreen].bounds.size;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    self.animator.presenting = YES;
    return self.animator;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    self.animator.presenting = NO;
    return self.animator;
}

@end
