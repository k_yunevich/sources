//
//  NSString+Size.m
//  KYAlertController
//
//  Created by Konstantin Yunevich on 05.02.2018.
//  Copyright © 2018 Konstantin Yunevich. All rights reserved.
//

#import "NSString+Size.h"

@implementation NSString (Size)

- (CGFloat)heightForWidth:(CGFloat)width font:(UIFont *)font {
    if (self.length == 0) {
        return 0;
    }
    
    CGSize size = CGSizeMake(width, CGFLOAT_MAX);
    NSDictionary *attributes = @{NSFontAttributeName : font};
    CGRect rect = [self boundingRectWithSize:size
                                     options:NSStringDrawingUsesLineFragmentOrigin
                                  attributes:attributes
                                     context:nil];
    return rect.size.height;
}

@end
