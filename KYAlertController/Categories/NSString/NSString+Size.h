//
//  NSString+Size.h
//  KYAlertController
//
//  Created by Konstantin Yunevich on 05.02.2018.
//  Copyright © 2018 Konstantin Yunevich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSString (Size)

- (CGFloat)heightForWidth:(CGFloat)width font:(UIFont *)font;

@end
