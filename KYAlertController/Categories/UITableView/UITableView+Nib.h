//
//  UITableView+Nib.h
//  KYAlertController
//
//  Created by Konstantin Yunevich on 04.02.2018.
//  Copyright © 2018 Konstantin Yunevich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (Nib)

- (UITableViewCell *)dequeueReusableCell:(Class)type forIndexPath:(NSIndexPath *)indexPath;
- (void)registerNibs:(NSArray<Class> *)types;
- (void)registerNib:(Class)type;

@end
