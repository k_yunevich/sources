//
//  UITableView+Nib.m
//  KYAlertController
//
//  Created by Konstantin Yunevich on 04.02.2018.
//  Copyright © 2018 Konstantin Yunevich. All rights reserved.
//

#import "UITableView+Nib.h"

static const NSString * kDefaultIdentifierPostfix = @"";

@implementation UITableView (Nib)

- (UITableViewCell *)dequeueReusableCell:(Class)type forIndexPath:(NSIndexPath *)indexPath {
    NSString *className = NSStringFromClass(type);
    NSString *identifier = [NSString stringWithFormat:@"%@%@", className, kDefaultIdentifierPostfix];
    return [self dequeueReusableCellWithIdentifier:identifier
                                      forIndexPath:indexPath];
}

- (void)registerNib:(Class)type {
    NSString *className = NSStringFromClass(type);
    UINib *nib = [UINib nibWithNibName:className bundle:nil];
    NSString *identifier = [NSString stringWithFormat:@"%@%@",className, kDefaultIdentifierPostfix];
    [self registerNib:nib forCellReuseIdentifier:identifier];
}

- (void)registerNibs:(NSArray<Class> *)types {
    for (Class type in types) {
        [self registerNib:type];
    }
}

@end
