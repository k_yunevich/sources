//
//  KYSelectAlertAnimator.m
//  KYAlertController
//
//  Created by Konstantin Yunevich on 04.02.2017.
//  Copyright © 2017 Konstantin Yunevich. All rights reserved.
//

#import "KYSelectAlertAnimator.h"

@interface KYSelectAlertAnimator ()

@property (strong, nonatomic) UIView *overlayView;

@end


@implementation KYSelectAlertAnimator

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return self.isPresenting ? 0.35 : 0.2;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    UIViewController* firstVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController* secondVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    UIView *containerView = [transitionContext containerView];
    UIView *firstView = firstVC.view;
    UIView *secondView = secondVC.view;
    CGRect screenRect = [UIScreen mainScreen].bounds;
    
    if (!self.overlayView) {
        UIView *overlayView = [[UIView alloc] initWithFrame:screenRect];
        overlayView.backgroundColor = [UIColor colorWithRed:4.0 / 255.0
                                                      green:4.0 / 255.0
                                                       blue:15.0 / 255.0
                                                      alpha:1.0];
        
        overlayView.alpha = 0.0;
        [containerView addSubview:overlayView];
        self.overlayView = overlayView;
    }
    
    if (self.isPresenting) {
        [containerView addSubview:secondView];
        CGRect secondViewFrame = CGRectMake(containerView.frame.origin.x,
                                            screenRect.size.height,
                                            containerView.frame.size.width,
                                            containerView.frame.size.height);
        secondView.frame = secondViewFrame;
        
        firstVC.view.tintAdjustmentMode = UIViewTintAdjustmentModeNormal;
        [UIView animateWithDuration:0.35 animations:^{
            self.overlayView.alpha = 0.6;
            secondView.frame = containerView.frame;
        } completion:^(BOOL finished) {
            [transitionContext completeTransition:YES];
        }];
    } else {
        CGRect firstViewFrame = CGRectMake(containerView.frame.origin.x,
                                           containerView.frame.origin.y + containerView.frame.size.height,
                                           containerView.frame.size.width,
                                           containerView.frame.size.height);
        
        [UIView animateWithDuration:0.2 animations:^{
            firstView.frame = firstViewFrame;
            self.overlayView.alpha = 0.0;
        } completion:^(BOOL finished) {
            [self.overlayView removeFromSuperview];
            self.overlayView = nil;
            [transitionContext completeTransition:YES];
        }];
    }
}


@end
