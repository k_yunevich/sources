//
//  Gallering.swift
//  Константин Юневич
//
//  Created by Константин Юневич on 22.07.16.
//  Copyright © 2016 Константин Юневич. All rights reserved.
//

import UIKit
import MobileCoreServices

enum GalleryAction {
    case makePhoto
    case albumPhoto
    case albumVideo
    case showPhoto
    case recordAudio
    case insert
    case delete
}

enum GallerySelectedType {
    case photo
    case audio
    case video
}

protocol Gallering: class {
    var output: UIViewController { get set }
    func galleryClosed()
    func galleryActionSheet(_ action: GalleryAction)
    func galleryItemSelected(_ item: AnyObject?, url: URL?, type: GallerySelectedType)
    func galleryActionSheetConfiguration() -> [GalleryAction]
    func galleryDeleteItemTitle() -> String
}

extension Gallering {
    
    func galleryActionSheetConfiguration() -> [GalleryAction] {
        return [.makePhoto, .albumPhoto, .insert]
    }
    
    func showGalleryActionSheet(withDelete delete: Bool = false) {
        let alert = UIAlertController(title: nil,
                                      message: nil,
                                      preferredStyle: UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet)
        
        let items = self.galleryActionSheetConfiguration()
        for item in items {
            switch item {
            case .makePhoto:
                alert.addAction(UIAlertAction(title: "Сделать фото",
                                              style: .default,
                                              handler: { [unowned self] (action) in
                                                self.galleryActionSheet(.makePhoto)
                                                self.openPhotoCamera()
                }))
            case .albumPhoto:
                alert.addAction(UIAlertAction(title: "Выбрать из галереи",
                                              style: .default,
                                              handler: { [weak self] (action) in
                                                self?.galleryActionSheet(.albumPhoto)
                                                self?.openPhotoGallery()
                }))
            case .albumVideo:
                alert.addAction(UIAlertAction(title: "Выбрать видео",
                                              style: .default,
                                              handler: { [unowned self] (action) in
                                                self.galleryActionSheet(.albumVideo)
                                                self.openVideoGallery()
                }))
            case .showPhoto:
                alert.addAction(UIAlertAction(title: "Показать фото",
                                              style: .default,
                                              handler: { [unowned self] (action) in
                                                self.galleryActionSheet(.showPhoto)
                }))
            case .recordAudio:
                alert.addAction(UIAlertAction(title: "Записать аудио",
                                              style: .default,
                                              handler: { [unowned self] (action) in
                                                self.galleryActionSheet(.recordAudio)
                }))
            case .insert:
                alert.addAction(UIAlertAction(title: "Вставить",
                                              style: .default,
                                              handler: { [unowned self] (action) in
                                                self.galleryActionSheet(.insert)
                                                self.insertPhoto()
                }))
            default: break
            }
        }
        
        if delete {
            alert.addAction(UIAlertAction(title: galleryDeleteItemTitle(),
                                          style: .destructive, handler: { [unowned self] (action) in
                                            self.galleryActionSheet(.delete)
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Отмена",
                                      style: .cancel, handler: { (action) in }))
        
        output.present(alert, animated: true, completion: nil)
    }
    
}

private var imAssociationKey: UInt8 = 11

extension Gallering {
    
    fileprivate var imageDelegate: UIImageViewControllerDelegate? {
        get {
            return objc_getAssociatedObject(self, &imAssociationKey) as? UIImageViewControllerDelegate
        }
        set {
            objc_setAssociatedObject(self, &imAssociationKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func openPhotoGallery() {
        openImagePicker(.photoLibrary)
    }
    
    func openVideoGallery() {
        openImagePicker(.photoLibrary, mediaTypes: [kUTTypeMovie as String])
    }
    
    func openPhotoCamera() {
        openImagePicker(.camera)
    }
    
    func insertPhoto() {
        let pastboard = UIPasteboard.general
        if let image = pastboard.image {
            galleryItemSelected(image, url: nil, type: .photo)
        }
        else {
            //showAlert(title: "Нет изображения в буфере")
        }
    }
    
    func closeGallery() {
        galleryClosed()
    }
    
    fileprivate func openImagePicker(_ sourceType: UIImagePickerControllerSourceType, mediaTypes: [String]? = [kUTTypeImage as String]) {
        guard UIImagePickerController.isSourceTypeAvailable(sourceType) else { return }
        
        if imageDelegate == nil {
            imageDelegate = UIImageViewControllerDelegate(selectedItem: { [weak self] (item, url, type) in
                self?.galleryItemSelected(item, url: url, type: type)
                }, closeGallery: { [weak self] in
                    self?.closeGallery()
            })
        }
        let picker = UIImagePickerController()
        picker.modalPresentationStyle = .currentContext
        picker.delegate = imageDelegate!
        picker.sourceType = sourceType
        picker.mediaTypes = mediaTypes!
        picker.allowsEditing = false
        picker.videoQuality = .typeMedium
        if let tabbar = output.tabBarController {
            tabbar.present(picker, animated: true, completion: nil)
        }  else {
            output.present(picker, animated: true, completion: nil)
        }
    }
    
}

class UIImageViewControllerDelegate: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    typealias ItemSelectedCompletion = (AnyObject?, URL?, GallerySelectedType) -> ()
    typealias GalleryCloseCompletion = () -> ()
    
    var selectedItem: ItemSelectedCompletion
    var closeGallery: GalleryCloseCompletion
    
    init(selectedItem: @escaping ItemSelectedCompletion, closeGallery: @escaping GalleryCloseCompletion) {
        self.selectedItem = selectedItem
        self.closeGallery = closeGallery
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : Any])
    {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage { 
            selectedItem(image, info[UIImagePickerControllerMediaURL] as? URL, .photo)
        } else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            selectedItem(image, info[UIImagePickerControllerMediaURL] as? URL, .photo)
        } else {
            let mediaType = info[UIImagePickerControllerMediaType] as! String
            let galleryType = galleryTypeByUTType(mediaType)
            selectedItem(nil, info[UIImagePickerControllerMediaURL] as? URL, galleryType)
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        closeGallery()
        picker.dismiss(animated: true, completion: nil)
    }
    
    func galleryTypeByUTType(_ UTType: String) -> GallerySelectedType {
        if UTTypeConformsTo(UTType as CFString, kUTTypeImage) {
            return .photo
        } else if UTTypeConformsTo(UTType as CFString, kUTTypeMovie) {
            return .video
        } else if UTTypeConformsTo(UTType as CFString, kUTTypeAudio) {
            return .audio
        } else {
            return .photo
        }
    }
}
