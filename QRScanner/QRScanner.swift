//
//  QRScanner.swift
//  Konstantin Yunevich
//
//  Created by Konstantin Yunevich on 24.01.2018.
//  Copyright © 2018 Konstantin Yunevich. All rights reserved.
//

import UIKit
import MTBBarcodeScanner

typealias QRScannerCompletionBlock = ((_ code: String?, _ scanner: QRScanner) -> Void)
typealias QRScannerFailureBlock = (NSError?, _ scanner: QRScanner) -> Void

class QRScanner: Alert {
    
    fileprivate var scanner: MTBBarcodeScanner!
    fileprivate var successBlock: QRScannerCompletionBlock
    fileprivate var failureBlock: QRScannerFailureBlock
    
    init(completion: @escaping QRScannerCompletionBlock, failure: @escaping QRScannerFailureBlock) {
        self.successBlock = completion
        self.failureBlock = failure
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black
        scanner = MTBBarcodeScanner(previewView: view)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        scanning { [unowned self] (code, error) in
            if let error = error {
                self.failureBlock(error, self)
                return
            }
            
            self.successBlock(code, self)
        }
    }
    
    func closeAndStop() {
        dismiss(animated: true, completion: { [unowned self] in
            self.scanner.stopScanning()
        })
    }
}

private extension QRScanner {
    func requestPermissions(completion: @escaping ((Bool) -> Void)) {
        MTBBarcodeScanner.requestCameraPermission(success: completion)
    }
    
    func scanning(completion: @escaping (_ code: String?, _ error: NSError?) -> Void) {
        requestPermissions { [weak self] (success) in
            guard let `self` = self else { return }
            guard success else {
                let userInfo = [NSLocalizedDescriptionKey: "Доступ к камере запрещен"]
                let error = NSError(domain: "", code: 0, userInfo: userInfo)
                completion(nil, error)
                return
            }
            
            do {
                try self.scanner.startScanning(resultBlock: { (codes) in
                    let code = codes?.first?.stringValue
                    completion(code, nil)
                })
            } catch let error {
                completion(nil, error as NSError)
            }
        }
    }
}
