//
//  Alert.swift
//  Konstantin Yunevich
//
//  Created by Konstantin Yunevich on 25.12.2017.
//  Copyright © 2017 GS. All rights reserved.
//

import UIKit

class Alert: UIViewController {
    fileprivate var animator: AlertAnimator = AlertAnimator()
    
    var height: CGFloat = 300
    
    init() {
        super.init(nibName: nil, bundle: nil)
        self.modalPresentationStyle = .custom
        self.transitioningDelegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.modalPresentationStyle = .custom
        self.transitioningDelegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewController()
    }
    
    func close(animated: Bool) {
        dismiss(animated: animated, completion: nil)
    }
}

// MARK: - Configuration
private extension Alert {
    func setupViewController() {
        view.backgroundColor = .white
        view.clipsToBounds = true
        view.layer.cornerRadius = 6
        
        animator.tapAction = { [unowned self] in
            self.close(animated: true)
        }
        
    }
}

// MARK: - Transitioning Delegate
extension Alert: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        animator.transitionType = .present
        animator.height = height
        return animator
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        animator.transitionType = .dismiss
        return animator
    }
}


