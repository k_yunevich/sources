//
//  AlertAnimator.swift
//  Konstantin Yunevich
//
//  Created by Konstantin Yunevich on 25.12.2017.
//  Copyright © 2017 Konstantin Yunevich. All rights reserved.
//

import UIKit

enum AlertTransitionType {
    case present
    case dismiss
}

class AlertAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    var transitionType: AlertTransitionType = .present
    var tapAction: (() -> Void)?
    
    // MARK: - Private properties
    fileprivate let presentDuration = 0.25
    fileprivate let dismissDuration = 0.15
    fileprivate var overlayView: UIView!
    
    var height: CGFloat = 300
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return transitionType == .present ? presentDuration : dismissDuration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let fromView        = transitionContext.view(forKey: .from)
        let toView          = transitionContext.view(forKey: .to)
        let containerView   = transitionContext.containerView
        let containerFrame  = containerView.frame
        let containerSize   = containerFrame.size
        
        if (overlayView == nil) {
            overlayView = UIView(frame: containerView.bounds)
            overlayView.backgroundColor = .black
            overlayView.alpha = 0.0
            let tapGesture = UITapGestureRecognizer(target: self,
                                                    action: #selector(overlayViewTapped(_:)))
            overlayView.addGestureRecognizer(tapGesture)
            containerView.addSubview(overlayView)
        }
        
        if (transitionType == .present) {
            guard let toView = toView else { return }
            containerView.addSubview(toView)
           
            let width = containerSize.width - 36
            let finishPositionX = containerSize.width     / 2 - width / 2
            let finishPositionY = containerSize.height    / 2 - self.height / 2
            
            let finishFrame = CGRect(x: finishPositionX,
                                     y: finishPositionY,
                                     width: width,
                                     height: self.height)
            
            toView.frame = finishFrame
            toView.alpha = 0.0
            
            UIView.animate(withDuration: presentDuration, delay: 0, options: [.curveEaseOut], animations: {
                self.overlayView.alpha = 0.4
                toView.alpha = 1.0
            }, completion: { (_) in
                transitionContext.completeTransition(true)
            })
            
        } else if (transitionType == .dismiss) {
            guard let fromView = fromView else { return }
            
            UIView.animate(withDuration: dismissDuration, delay: 0, options: [.curveEaseIn], animations: {
                self.overlayView.alpha = 0.0
                fromView.alpha = 0.0
            }, completion: { (_) in
                self.overlayView.removeFromSuperview()
                transitionContext.completeTransition(true)
            })
        }
    }
}

extension AlertAnimator {
    @objc func overlayViewTapped(_ gesture: UITapGestureRecognizer) {
        tapAction?()
    }
}
